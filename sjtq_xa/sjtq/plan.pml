<?xml version="1.0" encoding="GBK"?>
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="">
    <environment>
        <moduleId>
            <groupId>sjtq_xa</groupId>
            <artifactId>sjtq</artifactId>
            <version>1.0</version>
        </moduleId>
        <dependencies>
            <dependency>
                <groupId>SystemServer</groupId>
                <artifactId>base</artifactId>
                <version>1.0</version>
                <type>car</type>
            </dependency>
            <dependency>
                <groupId>SystemServer</groupId>
                <artifactId>connector</artifactId>
                <version>1.0</version>
                <type>car</type>
            </dependency>
        </dependencies>
        <non-overridable-classes/>
        <inverse-classloading/>
    </environment>
    <gbean class="cn.com.agree.eai.core.KernelBeanFactory" name="beanFactory">
        <reference name="ApplicationInfo">
            <type>GBean</type>
            <name>applicationInfo</name>
        </reference>
    </gbean>
    <gbean class="cn.com.agree.eai.script.pool.ScriptGenerator" name="scriptGenerator"/>
    <gbean class="cn.com.agree.eai.workflow.engine.JbpmWorkflow" name="workflow"/>
    <gbean class="cn.com.agree.eai.core.ApplicationInfo" name="applicationInfo">
        <attribute name="serverId" type="java.lang.String">sjtq_xa</attribute>
        <attribute name="moduleId" type="java.lang.String">sjtq</attribute>
        <attribute name="name" type="java.lang.String">sjtq</attribute>
        <attribute name="level" type="java.lang.String">debug</attribute>
        <attribute name="baseHome" type="java.lang.String"/>
    </gbean>
    <gbean class="cn.com.agree.eai.core.ExecutorManager" name="executorManager">
        <attribute name="maximumPoolSize" type="int">10</attribute>
        <attribute name="minimumPoolSize" type="int">5</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.core.GlobeSession" name="globeSession">
        <attribute name="timeout" type="long">6000000000</attribute>
    </gbean>
    <gbean
        class="cn.com.agree.eai.communication.in.ShortConnectionInAdaptor" name="shortConnectionInAdaptor">
        <attribute name="name" type="java.lang.String">sjtq</attribute>
        <attribute name="host" type="java.lang.String">0.0.0.0</attribute>
        <attribute name="port" type="int">8033</attribute>
        <attribute name="timeout" type="int">60000</attribute>
        <attribute name="ipFileName" type="java.lang.String"/>
        <attribute name="permit" type="boolean">true</attribute>
        <attribute name="reuseAddress" type="boolean">false</attribute>
        <reference name="ClientDispatcher">
            <type>GBean</type>
            <name>clientDispatcher</name>
        </reference>
    </gbean>
    <gbean class="cn.com.agree.eai.core.serial.KeyGeneratorFile" name="keyGenerator">
        <attribute name="interval" type="int">20</attribute>
        <attribute name="maxValue" type="long">1000000000</attribute>
        <attribute name="minValue" type="long">1</attribute>
        <attribute name="fileName" type="java.lang.String">ser.bin</attribute>
        <attribute name="formatStr" type="java.lang.String">00000000</attribute>
        <reference name="ApplicationInfo">
            <type>GBean</type>
            <name>applicationInfo</name>
        </reference>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="sqlw">
        <attribute name="username" type="java.lang.String">program</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;ZWkyOVZuMUl2MzVjNTFVWQ==</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.mysql.jdbc.Driver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:mysql://39.108.134.232:3306/CarStaionDB?useUnicode=true&amp;characterEncoding=UTF8</attribute>
        <attribute name="maxActive" type="int">5</attribute>
        <attribute name="minActive" type="int">2</attribute>
        <attribute name="maxIdle" type="int">3</attribute>
        <attribute name="minIdle" type="int">0</attribute>
        <attribute name="maxWait" type="long">60000</attribute>
        <attribute name="validationQuery" type="java.lang.String">select 1</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="sqlw2">
        <attribute name="username" type="java.lang.String">program</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;ZWkyOVZuMUl2MzVjNTFVWQ==</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.mysql.jdbc.Driver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:mysql://39.108.134.232:3306/che?useUnicode=true&amp;characterEncoding=UTF8</attribute>
        <attribute name="maxActive" type="int">5</attribute>
        <attribute name="minActive" type="int">2</attribute>
        <attribute name="maxIdle" type="int">3</attribute>
        <attribute name="minIdle" type="int">0</attribute>
        <attribute name="maxWait" type="long">60000</attribute>
        <attribute name="validationQuery" type="java.lang.String">select 1</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="sqlr">
        <attribute name="username" type="java.lang.String">root</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;cm9vdA==</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.mysql.jdbc.Driver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:mysql://192.166.1.254:3306/vehicle?useUnicode=true&amp;characterEncoding=UTF8</attribute>
        <attribute name="maxActive" type="int">5</attribute>
        <attribute name="minActive" type="int">2</attribute>
        <attribute name="maxIdle" type="int">3</attribute>
        <attribute name="minIdle" type="int">0</attribute>
        <attribute name="maxWait" type="long">60000</attribute>
        <attribute name="validationQuery" type="java.lang.String">select 1</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.core.dispatcher.ClientDispatcher" name="clientDispatcher">
        <attribute name="inputWorkflow" type="java.lang.String">input</attribute>
        <reference name="ApplicationInfo">
            <type>GBean</type>
            <name>applicationInfo</name>
        </reference>
        <reference name="KeyGenerator">
            <type>GBean</type>
            <name>keyGenerator</name>
        </reference>
        <reference name="GlobeSession">
            <type>GBean</type>
            <name>globeSession</name>
        </reference>
        <reference name="ExecutorManager">
            <type>GBean</type>
            <name>executorManager</name>
        </reference>
        <reference name="BeanFactory">
            <type>GBean</type>
            <name>beanFactory</name>
        </reference>
        <reference name="Workflow">
            <type>GBean</type>
            <name>workflow</name>
        </reference>
    </gbean>
    <gbean class="cn.com.agree.eai.core.dispatcher.SchedulerDispatcher" name="schedulerDispatcher">
        <attribute name="interval" type="long">600000</attribute>
        <attribute name="open" type="boolean">true</attribute>
        <reference name="ApplicationInfo">
            <type>GBean</type>
            <name>applicationInfo</name>
        </reference>
        <reference name="KeyGenerator">
            <type>GBean</type>
            <name>keyGenerator</name>
        </reference>
        <reference name="GlobeSession">
            <type>GBean</type>
            <name>globeSession</name>
        </reference>
        <reference name="ExecutorManager">
            <type>GBean</type>
            <name>executorManager</name>
        </reference>
        <reference name="BeanFactory">
            <type>GBean</type>
            <name>beanFactory</name>
        </reference>
        <reference name="Workflow">
            <type>GBean</type>
            <name>workflow</name>
        </reference>
    </gbean>
</module>
