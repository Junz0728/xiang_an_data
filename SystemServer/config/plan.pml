<?xml version="1.0" encoding="GBK"?>
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://geronimo.apache.org/xml/ns/deployment-1.1">
    <environment>
        <moduleId>
            <groupId>SystemServer</groupId>
            <artifactId>config</artifactId>
            <version>1.0</version>
        </moduleId>
        <dependencies>
            <dependency>
                <groupId>org.apache.geronimo.framework</groupId>
                <artifactId>j2ee-system</artifactId>
                <version>2.2</version>
                <type>car</type>
            </dependency>
            <dependency>
                <groupId>castor</groupId>
                <artifactId>castor</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>castor</groupId>
                <artifactId>castor-xml</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>castor</groupId>
                <artifactId>castor-core</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.apache.geronimo.specs</groupId>
                <artifactId>geronimo-servlet_2.5_spec</artifactId>
                <version>1.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jetty</groupId>
                <artifactId>jetty</artifactId>
                <version>6.1.22</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>jetty</groupId>
                <artifactId>jetty-util</artifactId>
                <version>6.1.22</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>afe</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>tools</artifactId>
                <version>1.0.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>afe</groupId>
                <artifactId>configServer</artifactId>
                <version>1.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>derby</groupId>
                <artifactId>derby</artifactId>
                <version>10.5.3.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>derby</groupId>
                <artifactId>derbyclient</artifactId>
                <version>10.5.3.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>derby</groupId>
                <artifactId>derbynet</artifactId>
                <version>10.5.3.0</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-dbcp</artifactId>
                <version>1.2.2</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-pool</artifactId>
                <version>1.5.4</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-collections</artifactId>
                <version>3.2.1</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>commons</groupId>
                <artifactId>commons-codec</artifactId>
                <version>1.3</version>
                <type>jar</type>
            </dependency>
            <dependency>
                <groupId>org.apache.xerces</groupId>
                <artifactId>xercesImpl</artifactId>
                <version>2.9.0</version>
                <type>jar</type>
            </dependency>
        </dependencies>
    </environment>
    <gbean class="cn.com.agree.eai.config.ConfigServer" name="configServer">
        <attribute name="port" type="int">3780</attribute>
        <attribute name="managed" type="boolean">false</attribute>
        <reference name="DBDatasourceGBean">
            <type>GBean</type>
            <name>dbDatasourceGBean</name>
        </reference>
    </gbean>
    <gbean class="cn.com.agree.eai.sql.datasource.DbcpDatasourceGBean" name="sybase_plat">
        <attribute name="username" type="java.lang.String">plat</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;MTIzNDU2</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.sybase.jdbc3.jdbc.SybDriver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:sybase:Tds:182.50.210.37:8888/platdb?charset=cp936</attribute>
        <attribute name="defaultAutoCommit" type="boolean">true</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="dbDatasourceGBean">
        <attribute name="username" type="java.lang.String">sa</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;MTIzNDU2</attribute>
        <attribute name="driverClassName" type="java.lang.String">org.apache.derby.jdbc.ClientDriver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:derby://127.0.0.1:1527/derbyDB;create=false</attribute>
        <attribute name="maxActive" type="int">5</attribute>
        <attribute name="minActive" type="int">2</attribute>
        <attribute name="maxIdle" type="int">3</attribute>
        <attribute name="minIdle" type="int">0</attribute>
        <attribute name="maxWait" type="long">60000</attribute>
        <attribute name="validationQuery" type="java.lang.String"/>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="sybase_plat1">
        <attribute name="username" type="java.lang.String">plat</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;MTIzNDU2</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.sybase.jdbc3.jdbc.SybDriver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:sybase:Tds:182.50.210.39:8888/platdb?charset=cp936</attribute>
        <attribute name="maxActive" type="int">5</attribute>
        <attribute name="minActive" type="int">2</attribute>
        <attribute name="maxIdle" type="int">3</attribute>
        <attribute name="minIdle" type="int">0</attribute>
        <attribute name="maxWait" type="long">10000</attribute>
        <attribute name="validationQuery" type="java.lang.String">set rowcount 1 select name from sysobjects where type='U'</attribute>
    </gbean>
    <gbean class="cn.com.agree.eai.config.DBDatasourceGBean" name="db2">
        <attribute name="username" type="java.lang.String">afe</attribute>
        <attribute name="password" type="java.lang.String">&gt;&gt;YWZl</attribute>
        <attribute name="driverClassName" type="java.lang.String">com.ibm.db2.jcc.DB2Driver</attribute>
        <attribute name="url" type="java.lang.String">jdbc:db2://182.50.210.42:33300/dbafe</attribute>
        <attribute name="maxActive" type="int">100</attribute>
        <attribute name="minActive" type="int">10</attribute>
        <attribute name="maxIdle" type="int">20</attribute>
        <attribute name="minIdle" type="int">10</attribute>
        <attribute name="maxWait" type="long">60000</attribute>
        <attribute name="validationQuery" type="java.lang.String">select * from sysibm.sysdummy1</attribute>
    </gbean>
</module>
