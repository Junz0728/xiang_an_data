<?xml version="1.0" encoding="GBK"?>
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://geronimo.apache.org/xml/ns/deployment-1.1 D:\geronimo-1.1\schema\geronimo-module-1.1.xsd">
	<environment>
		<moduleId>
			<groupId>SystemServer</groupId>
			<artifactId>derby</artifactId>
			<version>1.0</version>
		</moduleId>
		<dependencies>
			<dependency>
				<groupId>afe</groupId>
				<artifactId>afe</artifactId>
				<version>1.0</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>afe</groupId>
				<artifactId>tools</artifactId>
				<version>1.0.1</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>afe</groupId>
				<artifactId>configServer</artifactId>
				<version>1.0</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>derby</groupId>
				<artifactId>derby</artifactId>
				<version>10.5.3.0</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>derby</groupId>
				<artifactId>derbyclient</artifactId>
				<version>10.5.3.0</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>derby</groupId>
				<artifactId>derbynet</artifactId>
				<version>10.5.3.0</version>
				<type>jar</type>
			</dependency>
		</dependencies>
	</environment>
	<gbean class="cn.com.agree.eai.config.DerbyServer" name="derbyServer">
		<attribute name="host" type="java.lang.String">0.0.0.0</attribute>
		<attribute name="port" type="int">1527</attribute>
		<attribute name="maxThread" type="int">0</attribute>
	</gbean>
</module>
