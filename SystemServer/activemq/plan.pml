<?xml version="1.0" encoding="GB2312"?>
<!-- edited with XML Spy v4.4 U (http://www.xmlspy.com) by deng-cz (agree) -->
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1">
	<environment>
		<moduleId>
			<groupId>SystemServer</groupId>
			<artifactId>activemq</artifactId>
			<version>1.0</version>
		</moduleId>
		<dependencies>
			<dependency>
				<groupId>SystemServer</groupId>
				<artifactId>base</artifactId>
				<version>1.0</version>
				<type>car</type>
			</dependency>
		</dependencies>
	</environment>
	<gbean name="broker" class="cn.com.agree.eai.core.jms.broker.BrokerFactoryGBean">
		<attribute name="configUrl" type="java.lang.String">config/activemq.xml</attribute>
		<attribute name="start" type="boolean">true</attribute>
	</gbean>
</module>
