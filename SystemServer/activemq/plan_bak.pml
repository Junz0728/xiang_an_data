<?xml version="1.0" encoding="GB2312"?>
<!-- edited with XML Spy v4.4 U (http://www.xmlspy.com) by deng-cz (agree) -->
<module xmlns="http://geronimo.apache.org/xml/ns/deployment-1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://geronimo.apache.org/xml/ns/deployment-1.1
D:\geronimo-1.1\schema\geronimo-module-1.1.xsd">
	<environment>
		<moduleId>
			<groupId>SystemServer</groupId>
			<artifactId>activemq</artifactId>
			<version>1.0</version>
		</moduleId>
		<dependencies>
			<dependency>
				<groupId>org.apache.geronimo.specs</groupId>
				<artifactId>geronimo-jms_1.1_spec</artifactId>
				<version>1.0.1</version>
				<type>jar</type>
			</dependency>
			<!-- dependency>
				<groupId>commons-logging</groupId>
				<artifactId>commons-logging</artifactId>
				<version>1.0.4</version>
			</dependency>
			<dependency>
				<groupId>log4j</groupId>
				<artifactId>log4j</artifactId>
				<version>1.2.8</version>
			</dependency-->
			<dependency>
				<groupId>afe</groupId>
				<artifactId>afe</artifactId>
				<version>1.0</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>springframework</groupId>
				<artifactId>spring</artifactId>
				<version>1.2.5</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>activemq</groupId>
				<artifactId>activemq-core</artifactId>
				<version>4.1.1</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>activemq</groupId>
				<artifactId>activemq-console</artifactId>
				<version>4.1.1</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>activemq</groupId>
				<artifactId>backport-util-concurrent</artifactId>
				<version>2.1</version>
				<type>jar</type>
			</dependency>
			<!--dependency>
				<groupId>activemq</groupId>
				<artifactId>xbean-spring</artifactId>
				<version>2.2</version>
				<type>jar</type>
			</dependency-->
			<dependency>
				<groupId>activemq</groupId>
				<artifactId>xbean-spring</artifactId>
				<version>2.8</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>activemq</groupId>
				<artifactId>geronimo-j2ee-management_1.0_spec</artifactId>
				<version>1.0</version>
				<type>jar</type>
			</dependency>
			<dependency>
				<groupId>activeio</groupId>
				<artifactId>activeio-core</artifactId>
				<version>3.0.0-incubator</version>
				<type>jar</type>
			</dependency>
		</dependencies>
		<!-- hidden-classes>
			<filter>org.apache.log4j</filter>
		</hidden-classes-->
	</environment>
	<!-- gbean name="loggerManager" class="cn.com.agree.eai.core.LoggerManager">
		<attribute name="configFile">config/log4j.properties</attribute>
	</gbean-->
	<gbean name="broker" class="cn.com.agree.eai.core.jms.broker.BrokerFactoryGBean">
		<attribute name="configUrl" type="java.lang.String">config/activemq.xml</attribute>
		<attribute name="start" type="boolean">true</attribute>
	</gbean>
</module>
